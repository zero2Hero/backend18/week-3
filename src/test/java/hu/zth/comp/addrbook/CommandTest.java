package hu.zth.comp.addrbook;

import hu.zth.comp.addrbook.cli.command.Command;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {
    
    @Test
    @DisplayName("Command with multiple args")
    void test_command_with_multiple_args() {
        String[] args = {"cmd", "param 1", "param 2"};
        Command command = Command.of(args).ifError(Assertions::fail).getValue();

        assertAll(
                () -> assertNotNull(command),
                () -> assertNotNull(command.getCmd()),
                () -> assertNotNull(command.getParameters()),
                () -> assertEquals("cmd", command.getCmd()),
                () -> assertEquals(2, command.getParameters().size()),
                () -> assertIterableEquals(command.getParameters(), Arrays.asList("param 1", "param 2"))
        );
    }

    @Test
    @DisplayName("Command with two args")
    void test_command_with_two_args() {
        String[] args = {"cmd", "only param"};
        Command command = Command.of(args).ifError(Assertions::fail).getValue();

        assertAll(
                () -> assertNotNull(command),
                () -> assertNotNull(command.getCmd()),
                () -> assertNotNull(command.getParameters()),
                () -> assertEquals("cmd", command.getCmd()),
                () -> assertEquals(1, command.getParameters().size()),
                () -> assertIterableEquals(command.getParameters(), Collections.singletonList("only param"))
        );
    }

    @Test
    @DisplayName("Command with one arg")
    void test_command_with_one_arg() {
        String[] args = {"cmd"};
        Command command = Command.of(args).ifError(Assertions::fail).getValue();

        assertAll(
                () -> assertNotNull(command),
                () -> assertNotNull(command.getCmd()),
                () -> assertNotNull(command.getParameters()),
                () -> assertEquals("cmd", command.getCmd()),
                () -> assertEquals(0, command.getParameters().size())
        );
    }

    @Test
    @DisplayName("Command without any args")
    void test_command_without_arg() {
        String[] args = {};
        Command.of(args).ifPresentOrElse(
                command -> fail("Command instance is present, but is should not be"),
                error -> assertEquals("No command found, try \"help\"", error));
    }

    @Test
    @DisplayName("Command with empty arg")
    void test_command_with_empty_arg() {
        String[] args = {"  "};
        Command.of(args).ifPresentOrElse(
                command -> fail("Command instance is present, but is should not be"),
                error -> assertEquals("Empty command", error));
    }

    @Test
    @DisplayName("Command without null args")
    void test_command_with_null() {
        Command.of(null).ifPresentOrElse(
                command -> fail("Command instance is present, but is should not be"),
                error -> assertEquals("No command found, try \"help\"", error));
    }
    
}