package hu.zth.comp.addrbook;

import hu.zth.comp.addrbook.api.ContactElement;
import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;
import hu.zth.comp.addrbook.api.MutableContactElement;
import hu.zth.comp.addrbook.api.element.*;
import hu.zth.comp.addrbook.impl.element.DefaultAddressElement;
import hu.zth.comp.addrbook.impl.element.DefaultEmailElement;
import hu.zth.comp.addrbook.impl.element.DefaultNoteElement;
import hu.zth.comp.addrbook.impl.element.DefaultPhoneElement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ContactElementsTest {

    private MutableAddressBook addressBook;
    private MutableContact testObj;

    public ContactElementsTest() {
        addressBook = AddressBookTestHelper.createMutableAddressBook();
    }

    @BeforeEach
    void init() {
        testObj = addressBook.createContact();
    }

    @Test
    @DisplayName("Check empty elements")
    void test_check_empty_elements() {
        assertAll(
                () -> assertNotNull(testObj.getElements()),
                () -> assertTrue(testObj.getElements().isEmpty())
        );
    }

    @Test
    @DisplayName("Create address element by type name")
    void test_create_address_element_by_type_name() {
        MutableContactElement element = testObj.createElement(DefaultAddressElement.TYPE_NAME);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableAddressElement)
        );
    }

    @Test
    @DisplayName("Create address element by type name - ignore case")
    void test_create_address_element_by_type_name_ignore_case() {
        MutableContactElement element = testObj.createElement(DefaultAddressElement.TYPE_NAME.toLowerCase());
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableAddressElement),
                () -> assertEquals("Address", element.getElementType())
        );
    }

    @Test
    @DisplayName("Create address element by interface")
    void test_create_address_element_by_interface() {
        MutableContactElement element = testObj.createElement(AddressElement.class);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableAddressElement)
        );
    }

    @Test
    @DisplayName("Create email element by type name")
    void test_create_email_element_by_type_name() {
        MutableContactElement element = testObj.createElement(DefaultEmailElement.TYPE_NAME);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableEmailElement)
        );
    }

    @Test
    @DisplayName("Create email element by type name - ignore case")
    void test_create_email_element_by_type_name_ignore_case() {
        MutableContactElement element = testObj.createElement(DefaultEmailElement.TYPE_NAME.toLowerCase());
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableEmailElement),
                () -> assertEquals("E-Mail", element.getElementType())
        );
    }

    @Test
    @DisplayName("Create email element by interface")
    void test_create_email_element_by_interface() {
        MutableContactElement element = testObj.createElement(EmailElement.class);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableEmailElement)
        );
    }

    @Test
    @DisplayName("Create phone element by type name")
    void test_create_phone_element_by_type_name() {
        MutableContactElement element = testObj.createElement(DefaultPhoneElement.TYPE_NAME);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutablePhoneElement)
        );
    }

    @Test
    @DisplayName("Create phone element by type name - ignore case")
    void test_create_phone_element_by_type_name_ignore_case() {
        MutableContactElement element = testObj.createElement(DefaultPhoneElement.TYPE_NAME.toLowerCase());
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutablePhoneElement),
                () -> assertEquals("Phone", element.getElementType())
        );
    }

    @Test
    @DisplayName("Create phone element by interface")
    void test_create_phone_element_by_interface() {
        MutableContactElement element = testObj.createElement(PhoneElement.class);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutablePhoneElement)
        );
    }

    @Test
    @DisplayName("Create note element by type name")
    void test_create_note_element_by_type_name() {
        MutableContactElement element = testObj.createElement(DefaultNoteElement.TYPE_NAME);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableNoteElement)
        );
    }

    @Test
    @DisplayName("Create note element by type name - ignore case")
    void test_create_note_element_by_type_name_ignore_case() {
        MutableContactElement element = testObj.createElement(DefaultNoteElement.TYPE_NAME.toLowerCase());
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableNoteElement),
                () -> assertEquals("Note", element.getElementType())
        );
    }

    @Test
    @DisplayName("Create note element by interface")
    void test_create_note_element_by_interface() {
        MutableContactElement element = testObj.createElement(NoteElement.class);
        assertAll(
                () -> assertNotNull(element),
                () -> assertNotNull(element.getId()),
                () -> assertTrue(element.getId() > 0),
                () -> assertTrue(element instanceof MutableNoteElement)
        );
    }

    @Test
    @DisplayName("Create element by wrong type name")
    void test_create_address_element_by_worng_type_name() {
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> testObj.createElement("___Not-Exists___"));
        
        assertAll(
                () -> assertNotNull(ex),
                () -> assertEquals("Unknown contact element type: ___Not-Exists___", ex.getMessage())
        );
    }

    @Test
    @DisplayName("Create contact elements by list")
    void test_create_contact_elements_by_list() {
        Map<String, Class<? extends MutableContactElement>> mutableContactElementTypes = testObj.getMutableContactElementTypes();

        assertAll(
                () -> assertNotNull(mutableContactElementTypes),
                () -> assertFalse(mutableContactElementTypes.isEmpty())
        );

        mutableContactElementTypes.forEach(
                (typeName, typeClass) -> {
                    MutableContactElement elementByTypeName = testObj.createElement(typeName);
                    MutableContactElement elementByTypeClass = testObj.createElement(typeClass);

                    assertAll(
                            () -> assertNotNull(elementByTypeName),
                            () -> assertNotNull(elementByTypeClass),
                            () -> assertNotNull(elementByTypeName.getId()),
                            () -> assertNotNull(elementByTypeClass.getId()),
                            () -> assertNotSame(elementByTypeName, elementByTypeClass),
                            () -> assertSame(elementByTypeName.getClass(), elementByTypeClass.getClass()),
                            () -> assertEquals(typeName, elementByTypeName.getElementType()),
                            () -> assertEquals(typeName, elementByTypeClass.getElementType())
                    );
                }
        );
    }

    @Test
    @DisplayName("Create and add contact element")
    void test_create_and_add_contact_element() {
        MutableAddressElement addressElement = testObj.createElement(AddressElement.class);
        MutablePhoneElement phoneElementHome = testObj.createElement(PhoneElement.class);
        MutablePhoneElement phoneElementWork = testObj.createElement(PhoneElement.class);
        MutableEmailElement emailElement = testObj.createElement(EmailElement.class);

        addressElement.setCountry("Hungary");
        addressElement.setCity("Budapest");
        addressElement.setPostalCode("1111");
        addressElement.setAddress("Bartók Béla út 222");

        phoneElementHome.setPhone("+36 30 555 1234");

        phoneElementWork.setType(ContactElement.Type.WORK);
        phoneElementHome.setPhone("+36 20 444 4321");

        emailElement.setEmail("noreply@example.com");

        testObj.addElement(addressElement);
        testObj.addElement(phoneElementHome);
        testObj.addElement(phoneElementWork);
        testObj.addElement(emailElement);

        assertAll(
                () -> assertEquals(4, testObj.getElements().size()),
                () -> assertIterableEquals(testObj.getElements(), Arrays.asList(addressElement, phoneElementHome, phoneElementWork, emailElement)),
                () -> assertEquals("Hungary 1111 Budapest Bartók Béla út 222", addressElement.presentationString()),
                () -> assertEquals(ContactElement.Type.HOME, phoneElementHome.getType()),
                () -> assertEquals(ContactElement.Type.WORK, phoneElementWork.getType()),
                () -> assertEquals((Long) 1L, addressElement.getId()),
                () -> assertEquals((Long) 2L, phoneElementHome.getId()),
                () -> assertEquals((Long) 3L, phoneElementWork.getId()),
                () -> assertEquals((Long) 4L, emailElement.getId())
        );
    }

    @Test
    @DisplayName("Create add and delete contact element")
    void test_create_add_and_delete_contact_element() {
        MutableAddressElement addressElement = testObj.createElement(AddressElement.class);
        MutablePhoneElement phoneElementHome = testObj.createElement(PhoneElement.class);
        MutablePhoneElement phoneElementWork = testObj.createElement(PhoneElement.class);
        MutableEmailElement emailElement = testObj.createElement(EmailElement.class);

        addressElement.setCountry("Hungary");
        addressElement.setCity("Budapest");
        addressElement.setPostalCode("1111");
        addressElement.setAddress("Bartók Béla út 222");

        phoneElementHome.setPhone("+36 30 555 1234");

        phoneElementWork.setType(ContactElement.Type.WORK);
        phoneElementHome.setPhone("+36 20 444 4321");

        emailElement.setEmail("noreply@example.com");

        testObj.addElement(addressElement);
        testObj.addElement(phoneElementHome);
        testObj.addElement(phoneElementWork);
        testObj.addElement(emailElement);

        assertAll(
                () -> assertEquals(4, testObj.getElements().size()),
                () -> assertIterableEquals(testObj.getElements(), Arrays.asList(addressElement, phoneElementHome, phoneElementWork, emailElement)),
                () -> assertEquals("Hungary 1111 Budapest Bartók Béla út 222", addressElement.presentationString()),
                () -> assertEquals(ContactElement.Type.HOME, phoneElementHome.getType()),
                () -> assertEquals(ContactElement.Type.WORK, phoneElementWork.getType()),
                () -> assertEquals((Long) 1L, addressElement.getId()),
                () -> assertEquals((Long) 2L, phoneElementHome.getId()),
                () -> assertEquals((Long) 3L, phoneElementWork.getId()),
                () -> assertEquals((Long) 4L, emailElement.getId())
        );

        testObj.removeElement(phoneElementHome);

        assertAll(
                () -> assertEquals(3, testObj.getElements().size()),
                () -> assertIterableEquals(testObj.getElements(), Arrays.asList(addressElement, phoneElementWork, emailElement))
        );

        testObj.removeElement(addressElement);

        assertAll(
                () -> assertEquals(2, testObj.getElements().size()),
                () -> assertIterableEquals(testObj.getElements(), Arrays.asList(phoneElementWork, emailElement))
        );

        testObj.removeElement(phoneElementWork);
        testObj.removeElement(emailElement);

        assertAll(
                () -> assertEquals(0, testObj.getElements().size()),
                () -> assertTrue(testObj.getElements().isEmpty())
        );
    }

    @Disabled("Engedélyezd és implementált!!!")
    @Test
    @DisplayName("Contact element create and add multithread")
    void test_contact_element_create_and_add_multithread() {

    }

}