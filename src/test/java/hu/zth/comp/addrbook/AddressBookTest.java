package hu.zth.comp.addrbook;

import hu.zth.comp.addrbook.api.Contact;
import hu.zth.comp.addrbook.api.Identifiable;
import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookTest {

    private MutableAddressBook testObj;
    
    @BeforeEach
    public void init() {
        testObj = AddressBookTestHelper.createMutableAddressBook();
    }
    
    @Test
    @DisplayName("Create empty addressbook")
    void test_create_empty_addressbook() {
        assertAll(
                () -> assertNotNull(testObj),
                () -> assertEquals(0, testObj.size()),
                () -> assertTrue(testObj.getContacts().isEmpty())
        );
    }

    @Test
    @DisplayName("Create contact and do no add")
    void test_create_contact_and_do_no_add() {
        MutableContact contactAdded = testObj.createContact();
        contactAdded.setCompany("Microsoft");

        MutableContact contactNotAdded = testObj.createContact();
        contactNotAdded.setCompany("Apple");
        
        testObj.addContact(contactAdded);
        
        assertAll(
                () -> assertNotNull(contactNotAdded.getId()),
                () -> assertTrue(contactNotAdded.getId() > 0),
                () -> assertNotNull(contactAdded.getId()),
                () -> assertTrue(contactAdded.getId() > 0),
                () -> assertNotEquals(contactAdded.getId(), contactNotAdded.getId()),
                () -> assertEquals(1, testObj.size()),
                () -> assertTrue(testObj.getContacts().contains(contactAdded)),
                () -> assertFalse(testObj.getContacts().contains(contactNotAdded))
        );
    }

    @Test
    @DisplayName("Adding various company and name contacts")
    void test_adding_various_company_and_name_contacts() {
        MutableContact contact1 = testObj.createContact();
        contact1.setFirstName("John");
        contact1.setLastName("Doe");
        testObj.addContact(contact1);

        MutableContact contact2 = testObj.createContact();
        contact2.setCompany("Google");
        testObj.addContact(contact2);

        MutableContact contact3 = testObj.createContact();
        contact3.setLastName("Rambo");
        testObj.addContact(contact3);

        MutableContact contact4 = testObj.createContact();
        contact4.setFirstName("Ize");
        testObj.addContact(contact4);

        assertAll(
                () -> assertEquals((Long) 1L, contact1.getId()),
                () -> assertEquals((Long) 2L, contact2.getId()),
                () -> assertEquals((Long) 3L, contact3.getId()),
                () -> assertEquals((Long) 4L, contact4.getId()),
                () -> assertEquals(4, testObj.size()),
                () -> assertEquals(testObj.size(), testObj.getContacts().size()),
                () -> assertIterableEquals(testObj.getContacts(), Arrays.asList(contact1, contact2, contact3, contact4))
        );
    }

    @Test
    @DisplayName("Create contacts first and add all in the end")
    void test_create_contacts_first_and_add_all_in_the_end() {
        MutableContact contact1 = testObj.createContact();
        contact1.setFirstName("John");
        contact1.setLastName("Doe");

        MutableContact contact2 = testObj.createContact();
        contact2.setCompany("Google");
        
        MutableContact contact3 = testObj.createContact();
        contact3.setLastName("Rambo");
        
        MutableContact contact4 = testObj.createContact();
        contact4.setFirstName("Ize");
        
        testObj.addContact(contact1);
        testObj.addContact(contact2);
        testObj.addContact(contact3);
        testObj.addContact(contact4);

        assertAll(
                () -> assertEquals((Long) 1L, contact1.getId()),
                () -> assertEquals((Long) 2L, contact2.getId()),
                () -> assertEquals((Long) 3L, contact3.getId()),
                () -> assertEquals((Long) 4L, contact4.getId()),
                () -> assertEquals(4, testObj.size()),
                () -> assertEquals(testObj.size(), testObj.getContacts().size()),
                () -> assertIterableEquals(testObj.getContacts(), Arrays.asList(contact1, contact2, contact3, contact4))
        );
    }

    @Test
    @DisplayName("Remove contact")
    void test_remove_contact() {
        MutableContact contact1 = testObj.createContact();
        contact1.setFirstName("John");
        contact1.setLastName("Doe");
        testObj.addContact(contact1);

        MutableContact contact2 = testObj.createContact();
        contact2.setCompany("Google");
        testObj.addContact(contact2);

        MutableContact contact3 = testObj.createContact();
        contact3.setLastName("Rambo");
        testObj.addContact(contact3);

        MutableContact contact4 = testObj.createContact();
        contact4.setFirstName("Ize");
        testObj.addContact(contact4);

        MutableContact contact5 = testObj.createContact();
        contact5.setFirstName("ASD");
        
        boolean contact2removed = testObj.removeContact(contact2);
        boolean contact5removed = testObj.removeContact(contact5);

        assertAll(
                () -> assertEquals((Long) 1L, contact1.getId()),
                () -> assertEquals((Long) 2L, contact2.getId()),
                () -> assertEquals((Long) 3L, contact3.getId()),
                () -> assertEquals((Long) 4L, contact4.getId()),
                () -> assertEquals((Long) 5L, contact5.getId()),
                () -> assertTrue(contact2removed),
                () -> assertFalse(contact5removed),
                () -> assertEquals(3, testObj.size()),
                () -> assertIterableEquals(testObj.getContacts(), Arrays.asList(contact1, contact3, contact4))
        );
    }

    @Disabled("Engedélyezd és javítsd!!!")
    @Test
    @DisplayName("Contact create add multithread")
    void test_contact_create_add_multithread() throws InterruptedException {
        int taskCount = 1000;
        int threadCount = 16;
        int contactCountInOneThread = 4;
        
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        CountDownLatch countdown = new CountDownLatch(taskCount);
        
        for (int i = 0; i < taskCount; i++) {
            executorService.submit(() -> {
                Contact[] contacts = new Contact[contactCountInOneThread];
                
                for (int cc = 0; cc < contactCountInOneThread; cc++) {
                    contacts[cc] = testObj.createContact();
                }
                
                for (int cc = 0; cc < contactCountInOneThread; cc++) {
                    testObj.addContact(contacts[cc]);
                }

                countdown.countDown();
            });
        }
        
        countdown.await();
        executorService.shutdown();
        
        assertFalse(testObj.getContacts().isEmpty());
        
        String result = testObj.getContacts().stream().
                map(Identifiable::getId).
                collect(Collectors.groupingBy(id -> id, Collectors.counting())).
                entrySet().stream().
                filter(e -> e.getValue() > 1).
                map(e -> "id " + e.getKey() + " has been assigned " + e.getValue() + " times").
                collect(Collectors.joining("\n"));
        
        assertEquals("", result);
    }
    
}