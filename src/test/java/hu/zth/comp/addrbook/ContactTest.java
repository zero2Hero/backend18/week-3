package hu.zth.comp.addrbook;

import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContactTest {
    
    private MutableAddressBook addressBook;
    private MutableContact testObj;

    public ContactTest() {
        addressBook = AddressBookTestHelper.createMutableAddressBook();
    }

    @BeforeEach
    void init() {
        testObj = addressBook.createContact();
    }
    
    @Test
    @DisplayName("Contact name")
    void test_contact_name() {
        testObj.setFirstName("John");
        testObj.setLastName("Doe");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertTrue(testObj.getFirstName().isPresent()),
                () -> assertTrue(testObj.getLastName().isPresent()),
                () -> assertFalse(testObj.getCompany().isPresent()),
                () -> assertEquals("John", testObj.getFirstName().get()),
                () -> assertEquals("Doe", testObj.getLastName().get()),
                () -> assertEquals("John Doe", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with just first name")
    void test_contact_with_just_first_name() {
        testObj.setFirstName("John");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertTrue(testObj.getFirstName().isPresent()),
                () -> assertFalse(testObj.getLastName().isPresent()),
                () -> assertFalse(testObj.getCompany().isPresent()),
                () -> assertEquals("John", testObj.getFirstName().get()),
                () -> assertEquals("John", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with just last name")
    void test_contact_with_just_last_name() {
        testObj.setLastName("Doe");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertFalse(testObj.getFirstName().isPresent()),
                () -> assertTrue(testObj.getLastName().isPresent()),
                () -> assertFalse(testObj.getCompany().isPresent()),
                () -> assertEquals("Doe", testObj.getLastName().get()),
                () -> assertEquals("Doe", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with just the company")
    void test_contact_with_just_the_company() {
        testObj.setCompany("The Company");

        assertAll(
                () -> assertTrue(testObj.isCompany()),
                () -> assertFalse(testObj.getFirstName().isPresent()),
                () -> assertFalse(testObj.getLastName().isPresent()),
                () -> assertTrue(testObj.getCompany().isPresent()),
                () -> assertEquals("The Company", testObj.getCompany().get()),
                () -> assertEquals("", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with company and first name")
    void test_contact_with_company_and_first_name() {
        testObj.setFirstName("John");
        testObj.setCompany("IBM");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertTrue(testObj.getFirstName().isPresent()),
                () -> assertFalse(testObj.getLastName().isPresent()),
                () -> assertTrue(testObj.getCompany().isPresent()),
                () -> assertEquals("IBM", testObj.getCompany().get()),
                () -> assertEquals("John", testObj.getFirstName().get()),
                () -> assertEquals("John", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with company and last name")
    void test_contact_with_company_and_last_name() {
        testObj.setLastName("Doe");
        testObj.setCompany("IBM");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertFalse(testObj.getFirstName().isPresent()),
                () -> assertTrue(testObj.getLastName().isPresent()),
                () -> assertTrue(testObj.getCompany().isPresent()),
                () -> assertEquals("IBM", testObj.getCompany().get()),
                () -> assertEquals("Doe", testObj.getLastName().get()),
                () -> assertEquals("Doe", testObj.getFullName())
        );
    }

    @Test
    @DisplayName("Contact with company with first and last name")
    void test_contact_with_company_with_first_and_last_name() {
        testObj.setFirstName("John");
        testObj.setLastName("Doe");
        testObj.setCompany("IBM");

        assertAll(
                () -> assertFalse(testObj.isCompany()),
                () -> assertTrue(testObj.getFirstName().isPresent()),
                () -> assertTrue(testObj.getLastName().isPresent()),
                () -> assertTrue(testObj.getCompany().isPresent()),
                () -> assertEquals("IBM", testObj.getCompany().get()),
                () -> assertEquals("John", testObj.getFirstName().get()),
                () -> assertEquals("Doe", testObj.getLastName().get()),
                () -> assertEquals("John Doe", testObj.getFullName())
        );
    }
    
}