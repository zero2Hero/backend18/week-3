package hu.zth.comp.addrbook;

import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.impl.DefaultAddressBook;

final class AddressBookTestHelper {
    
    static MutableAddressBook createMutableAddressBook() {
        return new DefaultAddressBook();
    }
    
}
