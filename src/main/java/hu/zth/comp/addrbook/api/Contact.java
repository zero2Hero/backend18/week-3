package hu.zth.comp.addrbook.api;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Contact extends Serializable, Identifiable<Long> {
    
    Optional<String> getFirstName();

    Optional<String> getLastName();

    Optional<String> getCompany();
    
    List<ContactElement> getElements();
    
    default boolean hasElements() {
        return !getElements().isEmpty();
    }

    default boolean isCompany() {
        return getCompany().isPresent() &&
                !getFirstName().isPresent() &&
                !getLastName().isPresent();
    }

    default String getFullName() {
        return Stream.of(getFirstName(), getLastName()).
                filter(Optional::isPresent).
                map(Optional::get).
                collect(Collectors.joining(" "));
    }

}
