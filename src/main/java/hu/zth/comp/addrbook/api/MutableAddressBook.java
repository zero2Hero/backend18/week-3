package hu.zth.comp.addrbook.api;

public interface MutableAddressBook extends AddressBook {
    
    void addContact(Contact contact);
    
    boolean removeContact(Contact contact);
    
    MutableContact createContact();
    
}
