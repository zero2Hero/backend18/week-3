package hu.zth.comp.addrbook.api;

import java.io.Serializable;

public interface ContactElement extends Serializable, Identifiable<Long> {

    enum Type {
        HOME("Home"),
        WORK("Work");
        
        private String label;

        Type(String label) {
            this.label = label;
        }

        public String label() {
            return label;
        }
    }

    default Type getType() {
        return Type.HOME;
    }

    default String getElementType() {
        return this.getClass().getSimpleName();
    }
    
    String presentationString();
    
}
