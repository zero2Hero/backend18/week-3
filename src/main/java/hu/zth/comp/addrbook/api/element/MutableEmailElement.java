package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.MutableContactElement;

public interface MutableEmailElement extends EmailElement, MutableContactElement {
    
    void setEmail(String email);

}
