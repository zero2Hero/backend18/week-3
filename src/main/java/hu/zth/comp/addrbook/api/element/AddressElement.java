package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.ContactElement;

import java.util.Optional;

public interface AddressElement extends ContactElement {
    
    Optional<String> getCountry();

    Optional<String> getCity();

    Optional<String> getPostalCode();

    Optional<String> getAddress();
    
}
