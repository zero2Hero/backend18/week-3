package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.MutableContactElement;

public interface MutableNoteElement extends NoteElement, MutableContactElement {
    
    void setNote(String note);
    
}
