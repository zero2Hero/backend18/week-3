package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.MutableContactElement;

public interface MutableAddressElement extends AddressElement, MutableContactElement {

    void setCountry(String country);

    void setCity(String city);

    void setPostalCode(String postalCode);

    void setAddress(String address);

}
