package hu.zth.comp.addrbook.api;

public interface Identifiable<T> {

    T getId();
    
}
