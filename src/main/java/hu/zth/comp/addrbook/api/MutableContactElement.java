package hu.zth.comp.addrbook.api;

public interface MutableContactElement extends ContactElement {

    void setType(Type type);

}
