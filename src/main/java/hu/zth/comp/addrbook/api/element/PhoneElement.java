package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.ContactElement;

public interface PhoneElement extends ContactElement {
    
    String getPhone();

}
