package hu.zth.comp.addrbook.api;

import java.util.Map;

public interface MutableContact extends Contact {

    void setFirstName(String firstName);

    void setLastName(String lastName);

    void setCompany(String company);

    void addElement(ContactElement element);
    
    boolean removeElement(ContactElement element);

    <T extends MutableContactElement> T createElement(String typeName);

    <T extends MutableContactElement> T createElement(Class<? extends ContactElement> elementClass);

    Map<String, Class<? extends MutableContactElement>> getMutableContactElementTypes();
    
}
