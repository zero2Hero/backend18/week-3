package hu.zth.comp.addrbook.api;

import java.io.Serializable;
import java.util.List;

public interface AddressBook extends Serializable {
    
    List<Contact> getContacts();
    
    default int size() {
        return getContacts().size();
    }
    
}
