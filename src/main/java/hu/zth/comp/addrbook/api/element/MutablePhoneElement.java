package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.MutableContactElement;

public interface MutablePhoneElement extends PhoneElement, MutableContactElement {
    
    void setPhone(String phone);

}
