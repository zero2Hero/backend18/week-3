package hu.zth.comp.addrbook.api.element;

import hu.zth.comp.addrbook.api.ContactElement;

public interface NoteElement extends ContactElement {
    
    String getNote();
    
}
