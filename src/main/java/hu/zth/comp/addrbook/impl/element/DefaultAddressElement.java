package hu.zth.comp.addrbook.impl.element;

import hu.zth.comp.addrbook.api.element.MutableAddressElement;

import java.util.Optional;

public class DefaultAddressElement extends MutableTypedElement implements MutableAddressElement {
    
    public static final String TYPE_NAME = "Address";
    
    private String country;
    private String city;
    private String postalCode;
    private String address;

    public DefaultAddressElement(Long id) {
        super(id);
    }

    @Override
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public Optional<String> getCountry() {
        return Optional.ofNullable(country);
    }

    @Override
    public Optional<String> getCity() {
        return Optional.ofNullable(city);
    }

    @Override
    public Optional<String> getPostalCode() {
        return Optional.ofNullable(postalCode);
    }

    @Override
    public Optional<String> getAddress() {
        return Optional.ofNullable(address);
    }

    @Override
    public String presentationString() {
        StringBuilder sb = new StringBuilder();
        getCountry().ifPresent(c -> sb.append(c).append(" "));
        getPostalCode().ifPresent(p -> sb.append(p).append(" "));
        getCity().ifPresent(c -> sb.append(c).append(" "));
        getAddress().ifPresent(sb::append);
        return sb.toString();
    }

    @Override
    public String getElementType() {
        return TYPE_NAME;
    }
}
