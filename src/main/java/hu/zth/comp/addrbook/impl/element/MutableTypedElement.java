package hu.zth.comp.addrbook.impl.element;

import hu.zth.comp.addrbook.api.MutableContactElement;

public abstract class MutableTypedElement implements MutableContactElement {
    
    private Type type = Type.HOME;
    private Long id;

    public MutableTypedElement(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public Type getType() {
        return type;
    }
    
}
