package hu.zth.comp.addrbook.impl.element;

import hu.zth.comp.addrbook.api.element.MutableEmailElement;

public class DefaultEmailElement extends MutableTypedElement implements MutableEmailElement {

    public static final String TYPE_NAME = "E-Mail";
    
    private String email;

    public DefaultEmailElement(Long id) {
        super(id);
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String presentationString() {
        return email;
    }

    @Override
    public String getElementType() {
        return TYPE_NAME;
    }
}
