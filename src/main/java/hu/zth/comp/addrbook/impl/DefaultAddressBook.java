package hu.zth.comp.addrbook.impl;

import hu.zth.comp.addrbook.api.Contact;
import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultAddressBook implements MutableAddressBook {

    private Set<Contact> contacts = new HashSet<>();
    
    @Override
    public List<Contact> getContacts() {
        return contacts.stream().sorted(Comparator.comparing(Contact::getId)).collect(Collectors.toList());
    }

    @Override
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    @Override
    public boolean removeContact(Contact contact) {
        return contacts.remove(contact);
    }

    @Override
    public MutableContact createContact() {
        return new DefaultContact(getNextId());
    }

    private long getNextId() {
        return contacts.stream().
                mapToLong(Contact::getId).
                max().orElse(0L) + 1;
    }
    
}
