package hu.zth.comp.addrbook.impl.element;

import hu.zth.comp.addrbook.api.element.MutablePhoneElement;

public class DefaultPhoneElement extends MutableTypedElement implements MutablePhoneElement {

    public static final String TYPE_NAME = "Phone";
    
    private String phone;

    public DefaultPhoneElement(Long id) {
        super(id);
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String presentationString() {
        return phone;
    }

    @Override
    public String getElementType() {
        return TYPE_NAME;
    }
}
