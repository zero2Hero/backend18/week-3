package hu.zth.comp.addrbook.impl;

import hu.zth.comp.addrbook.api.ContactElement;
import hu.zth.comp.addrbook.api.MutableContact;
import hu.zth.comp.addrbook.api.MutableContactElement;
import hu.zth.comp.addrbook.impl.element.DefaultAddressElement;
import hu.zth.comp.addrbook.impl.element.DefaultEmailElement;
import hu.zth.comp.addrbook.impl.element.DefaultNoteElement;
import hu.zth.comp.addrbook.impl.element.DefaultPhoneElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

public class DefaultContact implements MutableContact {

    public static final Map<String, Class<? extends MutableContactElement>> CONTACT_ELEMENT_CLASSES;
    static {
        Map<String, Class<? extends MutableContactElement>> map = new HashMap<>();
        map.put(DefaultAddressElement.TYPE_NAME, DefaultAddressElement.class);
        map.put(DefaultEmailElement.TYPE_NAME, DefaultEmailElement.class);
        map.put(DefaultPhoneElement.TYPE_NAME, DefaultPhoneElement.class);
        map.put(DefaultNoteElement.TYPE_NAME, DefaultNoteElement.class);
        CONTACT_ELEMENT_CLASSES = Collections.unmodifiableMap(map);
    }

    private Long id;
    private String firstName;
    private String lastName;
    private String company;

    private Set<ContactElement> elements;

    public DefaultContact(Long id) {
        this.id = id;
        this.elements = new HashSet<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Optional<String> getFirstName() {
        return Optional.ofNullable(firstName);
    }

    @Override
    public Optional<String> getLastName() {
        return Optional.ofNullable(lastName);
    }

    @Override
    public Optional<String> getCompany() {
        return Optional.ofNullable(company);
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public List<ContactElement> getElements() {
        return elements.stream().sorted(Comparator.comparing(ContactElement::getId)).collect(Collectors.toList());
    }

    @Override
    public void addElement(ContactElement element) {
        elements.add(element);
    }

    @Override
    public boolean removeElement(ContactElement element) {
        return elements.remove(element);
    }

    @Override
    public Map<String, Class<? extends MutableContactElement>> getMutableContactElementTypes() {
        return CONTACT_ELEMENT_CLASSES;
    }

    @Override
    public <T extends MutableContactElement> T createElement(String typeName) {
        return createElement(CONTACT_ELEMENT_CLASSES.get(typeName));
    }
    
    @Override
    public <T extends MutableContactElement> T createElement(Class<? extends ContactElement> elementClass) {
        Optional<Class<? extends MutableContactElement>> clz =
                CONTACT_ELEMENT_CLASSES.values().stream().filter(elementClass::isAssignableFrom).findFirst();
        
        if (clz.isPresent()) {
            return createMutableContactElement(clz.get());
        }
        
        throw new IllegalStateException("Could not found implementation for element type: " + elementClass.getName());
    }
    
    private long getNextId() {
        return elements.stream().
                mapToLong(ContactElement::getId).
                max().orElse(0L) + 1;
    }

    @SuppressWarnings("unchecked")
    private <T extends MutableContactElement> T createMutableContactElement(Class<? extends MutableContactElement> clz) {
        try {
            Constructor<? extends MutableContactElement> constructor = clz.getConstructor(Long.class);
            return (T) constructor.newInstance(getNextId());
        } catch (IllegalAccessException | InstantiationException |
                NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalStateException("Could not create element class of type: " + clz.getName(), e);
        }
    }
}
