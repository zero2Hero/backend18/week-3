package hu.zth.comp.addrbook.impl.element;

import hu.zth.comp.addrbook.api.element.MutableNoteElement;

public class DefaultNoteElement extends MutableTypedElement implements MutableNoteElement {

    public static final String TYPE_NAME = "Note";
    
    private String note;

    public DefaultNoteElement(Long id) {
        super(id);
    }

    @Override
    public String getNote() {
        return note;
    }

    @Override
    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String presentationString() {
        return note;
    }

    @Override
    public String getElementType() {
        return TYPE_NAME;
    }
}
