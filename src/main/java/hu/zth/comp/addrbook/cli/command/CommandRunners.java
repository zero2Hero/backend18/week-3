package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.cli.Either;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class CommandRunners {

    private static final Map<String, CommandRunner> RUNNERS;
    
    private CommandRunners() {
        throw new UnsupportedOperationException();
    }

    static {
        Map<String, CommandRunner> runners = new HashMap<>();
        runners.put("help", new HelpRunner());
        runners.put("list", new ListRunner());
        runners.put("new", new NewContactRunner());
        runners.put("del", new DelContactRunner());
        runners.put("add", new AddRunner());
        RUNNERS = Collections.unmodifiableMap(runners);
    }

    public static Either<CommandRunner> getByCommand(Command command) {
        return Either.ofNullable(RUNNERS.get(command.getCmd()), () -> "Unknown command \"" + command.getCmd() + "\", try \"help\"");
    }

}
