package hu.zth.comp.addrbook.cli;

import java.util.function.Consumer;
import java.util.function.Supplier;

public interface Either<T> {
    
    T getValue();
    
    String getError();
    
    default boolean isPresent() {
        return getValue() != null;
    }
    
    default Either<T> ifPresent(Consumer<T> consumer) {
        if (isPresent()) {
            consumer.accept(this.getValue());
        }
        return this;
    }

    default Either<T> ifError(Consumer<String> consumer) {
        if (!isPresent()) {
            consumer.accept(this.getError());
        }
        return this;
    }

    default Either<T> ifPresentOrElse(Consumer<T> presentConsumer, Consumer<String> elseConsumer) {
        if (isPresent()) {
            presentConsumer.accept(getValue());
        } else {
            elseConsumer.accept(getError());
        }
        return this;
    }
    
    static <T> Either<T> of(T value, String error) {
        return new Either<T>() {
            @Override
            public T getValue() {
                return value;
            }

            @Override
            public String getError() {
                return error;
            }
        };
    }
    
    static <T> Either<T> ofNullable(T value, Supplier<String> errorSupplier) {
        return new Either<T>() {
            @Override
            public T getValue() {
                return value;
            }

            @Override
            public String getError() {
                return value == null ? errorSupplier.get() : null;
            }
        };
    }
    
    static <T> Either<T> of(T value) {
        return of(value, null);
    }

    static <T> Either<T> error(String error) {
        return of(null, error);
    }
    
}
