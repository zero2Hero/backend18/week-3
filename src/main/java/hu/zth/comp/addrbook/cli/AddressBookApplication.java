package hu.zth.comp.addrbook.cli;

import hu.zth.comp.addrbook.api.AddressBook;
import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.cli.command.Command;
import hu.zth.comp.addrbook.cli.command.CommandRunners;
import hu.zth.comp.addrbook.impl.DefaultAddressBook;

import java.io.*;

@SuppressWarnings("squid:S106")
public final class AddressBookApplication {

    private static final String ADDRESS_BOOK_FILE = "addressbook.ser";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MutableAddressBook addressBook = loadAddressBook();

        Command.of(args).ifPresentOrElse(
                command -> CommandRunners.getByCommand(command).
                        ifPresentOrElse(
                                commandRunner -> commandRunner.run(command, addressBook),
                                System.err::println
                        ),
                System.err::println);

        saveAddressBook(addressBook);
    }

    private static MutableAddressBook loadAddressBook() throws IOException, ClassNotFoundException {
        File addressBookFile = new File(ADDRESS_BOOK_FILE);
        if (addressBookFile.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(addressBookFile))) {
                return (MutableAddressBook) ois.readObject();
            }
        }
        return new DefaultAddressBook();
    }

    private static void saveAddressBook(AddressBook addressBook) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ADDRESS_BOOK_FILE))) {
            oos.writeObject(addressBook);
        }
    }

}
