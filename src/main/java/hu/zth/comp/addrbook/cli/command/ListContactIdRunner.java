package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.Contact;
import hu.zth.comp.addrbook.api.MutableAddressBook;

import java.util.Optional;

@SuppressWarnings("squid:S106")
public class ListContactIdRunner extends ContactAwareCommandRunner {

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        String param = command.getParameters().get(0);
        try {
            Long id = Long.parseLong(param);
            Optional<Contact> contact = addressBook.getContacts().stream().filter(c -> c.getId().equals(id)).findFirst();
            if (contact.isPresent()) {
                System.out.println(String.format("Address book contact (id: %d):", id));
                printContact(contact.get());
                printSeparatorLine();
            } else {
                System.err.println("Cannot found contact with id: " + id);
            }
        } catch (NumberFormatException e) {
            System.err.println("Invalid id: " + param);
        }
    }
}
