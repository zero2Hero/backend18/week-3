package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.AddressBook;
import hu.zth.comp.addrbook.api.Contact;

import java.util.Optional;

@SuppressWarnings("squid:S106")
public abstract class ContactAwareCommandRunner implements CommandRunner {

    void printContact(Contact contact) {
        System.out.println(String.format("Contact [% 3d]", contact.getId()));
        indented("Name: " + getContactName(contact));
        indentedSeparatorShort();
        contact.getElements().forEach(
                element -> indented(
                        String.format("[% 3d] %-10s (%-8s): %s",
                                element.getId(), element.getElementType(),
                                element.getType().label(), element.presentationString())));
        printSeparatorLine();
    }
    
    Optional<Contact> getContactById(AddressBook addressBook, Long id) {
        return addressBook.getContacts().stream().filter(c -> c.getId().equals(id)).findFirst();
    }
 
    private String getContactName(Contact contact) {
        if (contact.isCompany()) {
            Optional<String> company = contact.getCompany();
            if (company.isPresent()) {
                return company.get() + " (company)";
            }
        }
        return contact.getFullName();
    }
}
