package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;

@SuppressWarnings("squid:S106")
public class ListContactsRunner extends ContactAwareCommandRunner {

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        System.out.println("Address book size: " + addressBook.size());
        printSeparatorLine();
        addressBook.getContacts().forEach(this::printContact);
    }
}
