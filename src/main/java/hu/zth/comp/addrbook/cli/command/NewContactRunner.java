package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;

@SuppressWarnings("squid:S106")
public class NewContactRunner implements CommandRunner {

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        if (command.getParameters().isEmpty()) {
            System.err.println("Command new requires at least one parameter");
        } else {
            MutableContact contact = addressBook.createContact();
            if (command.getParameters().size() > 1) {
                String firstName = command.getParameters().get(0);
                String lastName = command.getParameters().get(1);
                contact.setFirstName(firstName);
                contact.setLastName(lastName);
            } else {
                String companyName = command.getParameters().get(0);
                contact.setCompany(companyName);
            }
            addressBook.addContact(contact);
            System.out.println("Contact added with ID: " + contact.getId());
        }
    }
    
}
