package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.cli.Either;

import java.util.*;

public final class Command {
    
    private String cmd;
    private List<String> parameters;
    
    private Command(String cmd, List<String> parameters) {
        this.cmd = cmd;
        this.parameters = Collections.unmodifiableList(parameters);
    }
    
    public String getCmd() {
        return cmd;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public static Either<Command> of(String[] args) {
        if (args != null && args.length > 0) {
            List<String> argList = Arrays.asList(args);
            String command = argList.remove(0).trim();
            return command.isEmpty() ?
                    Either.error("Empty command") :
                    Either.of(new Command(command, argList));
        } else {
            return Either.error("No command found, try \"help\"");
        }
    }
}
