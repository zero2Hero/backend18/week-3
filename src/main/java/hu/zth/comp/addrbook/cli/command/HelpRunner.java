package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;

@SuppressWarnings("squid:S106")
public class HelpRunner implements CommandRunner {

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        System.err.println("Available commands:");
        System.err.println(" - help");
        System.err.println(" - list");
        System.err.println(" - list types");
        System.err.println(" - list <contact id>");
        System.err.println(" - new <first name> <last name>");
        System.err.println(" - new <company name>");
        System.err.println(" - add <contact id> <element type> [-help] <params>");
        System.err.println(" - del <contact id>");
        System.err.println(" - del <contant_id> <element id>");
    }
    
}
