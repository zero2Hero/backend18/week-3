package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.impl.DefaultContact;

@SuppressWarnings("squid:S106")
public class ListTypesRunner implements CommandRunner {

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        System.out.println("Address book contact element types:");
        printSeparatorLine();
        DefaultContact.CONTACT_ELEMENT_CLASSES.entrySet().stream().
                map(e -> String.format("  %-12s - %s", e.getKey(), e.getValue().getName())).
                forEach(System.out::println);
    }
}
