package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.Contact;
import hu.zth.comp.addrbook.api.ContactElement;
import hu.zth.comp.addrbook.api.MutableAddressBook;
import hu.zth.comp.addrbook.api.MutableContact;

import java.util.Optional;

@SuppressWarnings("squid:S106")
public class DelContactRunner extends ContactAwareCommandRunner {
    
    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        if (command.getParameters().isEmpty()) {
            System.err.println("Command del requires at least one parameter, see \"help\"");
        } else {
            String contactIdParam = command.getParameters().get(0);
            try {
                long contactId = Long.parseLong(contactIdParam);
                Optional<Contact> contactOptional = getContactById(addressBook, contactId);
                if (contactOptional.isPresent()) {
                    removeContact(command, addressBook, contactOptional.get());
                } else {
                    System.err.println("No contact found with ID: " + contactId);
                }
            } catch (NumberFormatException e) {
                System.err.println("Invalid id: " + contactIdParam);
            }
        }
    }

    private void removeContact(Command command, MutableAddressBook addressBook, Contact foundContact) {
        long contactId = foundContact.getId();
        if (foundContact instanceof MutableContact) {
            MutableContact contact;
            contact = (MutableContact) foundContact;
            if (command.getParameters().size() > 1) {
                removeContactElement(command, contact);
            } else {
                if (addressBook.removeContact(contact)) {
                    System.out.println("Contact with ID: " + contactId + " has been removed.");
                } else {
                    System.out.println("Could not remove contact with ID: " + contactId);
                }
            }
        } else {
            System.err.println("Found contact (" + contactId + ") is not a modifiable contact");
        }
    }

    private void removeContactElement(Command command, MutableContact contact) {
        String elemIdParam = command.getParameters().get(1);
        try {
            long elemId = Long.parseLong(elemIdParam);
            Optional<ContactElement> contactElement = contact.getElements().stream().filter(e -> elemId == e.getId()).findFirst();
            if (contactElement.isPresent()) {
                contact.removeElement(contactElement.get());
            } else {
                System.err.println("No contactElement with ID: " + elemId + " has been found for contact (" + contact.getId() + ")");
            }
        } catch (NumberFormatException e) {
            System.err.println("Invalid elemenet id: " + elemIdParam);
        }
    }

}
