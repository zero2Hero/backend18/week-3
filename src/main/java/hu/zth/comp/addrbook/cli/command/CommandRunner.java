package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;

@SuppressWarnings("squid:S106")
public interface CommandRunner {
    
    void run(Command command, MutableAddressBook addressBook);
    
    default void printSeparatorLine() {
        System.out.println("----------------------------------------------------------------------");
    }
    
    default void indented(String output) {
        System.out.println("    " + output);
    }
    
    default void indentedSeparatorShort() {
        indented("-----");
    }

}
