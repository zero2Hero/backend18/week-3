package hu.zth.comp.addrbook.cli.command;

import hu.zth.comp.addrbook.api.MutableAddressBook;

public class ListRunner implements CommandRunner {

    private CommandRunner listTypesRunner;
    private CommandRunner listContactsRunner;
    private CommandRunner listContactIdRunner;

    public ListRunner() {
        listTypesRunner = new ListTypesRunner();
        listContactsRunner = new ListContactsRunner();
        listContactIdRunner = new ListContactIdRunner();
    }

    @Override
    public void run(Command command, MutableAddressBook addressBook) {
        if (command.getParameters().isEmpty()) {
            listContactsRunner.run(command, addressBook);
        } else {
            String param = command.getParameters().get(0);
            if ("types".equalsIgnoreCase(param)) {
                listTypesRunner.run(command, addressBook);
            } else {
                listContactIdRunner.run(command, addressBook);
            }
        }
    }

}
